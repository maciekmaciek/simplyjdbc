package model;

public class User {

    private String name;
    private int memberID;
    private static int ID = 0;


    public User(String name) {
        this.name = name;
        this.memberID = ID++;
    }

    public User(int memberID, String name) {
        this.name = name;
        this.memberID = memberID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public static int getID() {
        return ID;
    }

    public static void setID(int ID) {
        User.ID = ID;
    }

    @Override
    public String toString() {
        return "User: " + name +" ID:"+ memberID;
    }
}
