import data.JDBCQueryCreator;
import model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class RunApplication {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        Menu();


    }
    private static void Menu() throws SQLException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        int exit = 0;
        int choose = -1;
        ChooseMenu();
        while (choose != exit) {
            choose = sc.nextInt();
            switch (choose) {
                case 1:
                    JDBCQueryCreator.createTableUser();
                    ChooseMenu();
                    break;
                case 2:
                    System.out.println("Set name: ");
                    User user = new User(sc.next());
                    JDBCQueryCreator.insertUserToData(user);
                    ChooseMenu();
                    break;
                case 3:
                    List<User> allUsers = JDBCQueryCreator.getAllUsers();
                    allUsers.forEach(System.out::println);
                    ChooseMenu();
                    break;
                case 4:
                    System.out.println("a");
                    ChooseMenu();
                    break;
                case 0:
                    System.out.println("exit, bye ! ");
                    JDBCQueryCreator.dropTable();
                    break;
                default:
                    System.out.println("wrong choose");
            }


        }

    }

    private static void ChooseMenu() {
        System.out.println("Welcome, choose what do you want to do: ");
        System.out.println("1. Create new table");
        System.out.println("2. Insert in to table");
        System.out.println("3. Get all from table");
        System.out.println("4. Delete all from table");
        System.out.println("0. Exit");
    }
}
