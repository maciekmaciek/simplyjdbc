package data;

import model.User;
import service.DataBaseConfig;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JDBCQueryCreator {

    public static void createTableUser() throws SQLException, ClassNotFoundException {
        Connection connection = DataBaseConfig.getDBConnection();
        Statement statement = connection.createStatement();
        String createTable = "CREATE TABLE USER(id int primary key, name varchar(255))";
        statement.execute(createTable);
    }

    public static void insertUserToData(User user) throws SQLException, ClassNotFoundException {
        Connection connection = DataBaseConfig.getDBConnection();
        Statement statement = connection.createStatement();
        int id = user.getMemberID();
        String username = user.getName();
        String insertUser = "INSERT INTO USER(id, name) values ('" + id + "','" + username + "') ";
        statement.execute(insertUser);

    }

    public static List<User> getAllUsers() throws SQLException, ClassNotFoundException {
        List<User> users = new ArrayList<User>();
        Connection connection = DataBaseConfig.getDBConnection();
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("SELECT * FROM USER");

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");

            users.add(new User(id, name));
        }
        return users;
    }

    public static void dropTable() throws SQLException, ClassNotFoundException {
        Connection connection = DataBaseConfig.getDBConnection();
        Statement statement = connection.createStatement();
        String dropDataBase = "DROP TABLE USER";
        statement.executeUpdate(dropDataBase);

    }
}
