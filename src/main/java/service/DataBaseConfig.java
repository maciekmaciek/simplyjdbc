package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConfig {

    private static String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static String MYSQLDRIVER = "jdbc:mysql://localhost/DbTestJDBC?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static String USER = "root";
    private static String PASSWORD = "password";

     public static Connection getDBConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName(DRIVER);
        connection = DriverManager.getConnection(MYSQLDRIVER, USER, PASSWORD);
        return connection;

    }


}
